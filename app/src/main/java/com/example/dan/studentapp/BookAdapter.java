package com.example.dan.studentapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder>
{
    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the books in a ArrayList
    private ArrayList<BookSelect> items;

    //getting the context and book arraylist with constructor
    public BookAdapter(Context mCtx, ArrayList<BookSelect> items ) {
        this.mCtx = mCtx;
        this.items = items;
}


    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_products, null);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  BookViewHolder holder, int position)
    {
//getting the product of the specified position
        BookSelect book = items.get(position);

        //binding the data with the viewholder views
        holder.textViewcategory.setText(String.valueOf(book.getCategory()));
        holder.textViewtitle.setText(String.valueOf(book.getBtitle()));
        holder.textViewauthor.setText(String.valueOf(book.getAuthor()));
        holder.textViewedition.setText(String.valueOf(book.getEdition()));
        holder.textViewisbn.setText(String.valueOf(book.getIsbn()));
}

    @Override
    public int getItemCount() {
        return items.size();
    }
    class BookViewHolder extends RecyclerView.ViewHolder {

        TextView textViewcategory,textViewtitle,textViewauthor,textViewedition, textViewisbn  ;


        public BookViewHolder(View itemView) {
            super(itemView);

            textViewcategory = itemView.findViewById(R.id.textViewcategory);
            textViewtitle = itemView.findViewById(R.id.textViewtitle);
            textViewauthor  = itemView.findViewById(R.id.textViewauthor);
            textViewedition = itemView.findViewById(R.id.textViewedition);
            textViewisbn = itemView.findViewById(R.id.textViewisbn);
        }
    }
}
