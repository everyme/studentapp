package com.example.dan.studentapp;

public class BookSelect {

    private String category;
    private String btitle;
    private String author;
    private String edition;
    private String isbn;

    public BookSelect(String category, String btitle, String author, String edition, String isbn) {
        this.category = category;
        this.btitle = btitle;
        this.author = author;
        this.edition = edition;
        this.isbn = isbn;
    }


    public String getCategory() {
        return category;
    }

    public String getBtitle() {
        return btitle;
    }

    public String getAuthor() {
        return author;
    }

    public String getEdition() {
        return edition;
    }

    public String getIsbn() {
        return isbn;
    }




    }



