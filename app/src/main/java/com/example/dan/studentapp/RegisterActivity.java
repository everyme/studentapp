package com.example.dan.studentapp;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity  {

    EditText editSid, editFn, editSn, editPw, editEmail;
    Button btnReg, btnCanc;
    Spinner sGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editSid = (EditText) findViewById(R.id.etSid);
        editFn = (EditText) findViewById(R.id.etFn);
        editSn = (EditText) findViewById(R.id.etSn);
        sGroup = (Spinner) findViewById(R.id.groupspin);//to obtain example of spinner
        editEmail = (EditText) findViewById(R.id.etEmail);
        editPw = (EditText) findViewById(R.id.etPw);
        btnReg = (Button) findViewById(R.id.bReg);
        // btnCanc = (button) findViewById(R.id.bCancel);
        //setting of adapter to fill from values folder, to roll down
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(this, R.array.group_array,android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sGroup.setAdapter(adapter);// to call adapter
        //to obtain information selected from the list  of spinner and handle it осторожно!!!!
        sGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View itemSelected, int selectedItemPosition, long selectedId ) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//if group not selected then btn cancel and intent to close app
            }
        });



        //активация процесса регистрации
        btnReg.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          if (editFn.getText().toString() != "" && editSn.getText().toString() != ""
                                                  && editEmail.getText().toString() != "" && editPw.getText().toString() != "") {
                                              SQLCon con = new SQLCon(RegisterActivity.this);
                                              SQLiteDatabase db = con.getWritableDatabase();
                                              String _fn = editFn.getText().toString();
                                              String _sn = editSn.getText().toString();
                                              String _groupChoice = sGroup.getSelectedItem().toString();
                                              String _em = editEmail.getText().toString();
                                              String _pw = editPw.getText().toString();
                                              con.insertData(db, _fn, _sn, _groupChoice, _em, _pw);
                                              con.close();
                                          } else {
                                              Toast.makeText(getApplicationContext(), "Kindly fill in all required info", Toast.LENGTH_LONG).show();
                                          }
                                      }
                                  }
        );


    }
}

