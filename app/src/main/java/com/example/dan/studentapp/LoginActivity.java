package com.example.dan.studentapp;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity
{
    private EditText editEmail;
    private EditText editPw;
    private Button bLogin;
    private Button bRegister;
    private View.OnClickListener mOnLoginClickListener = new View.OnClickListener() {


        @Override
        public void onClick(View v) {

            String s1 = editEmail.getText().toString();
            String s2 =editPw.getText().toString();
            if (isEmailValid() && isPasswordValid()) {
                SQLCon con = new SQLCon(LoginActivity.this);
                SQLiteDatabase db = con.getWritableDatabase();
                Boolean checkData = con.checkData(db,s1,s2);
                if (checkData == true) {
                    Toast.makeText(getApplicationContext(), "Registered successfully", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Please register", Toast.LENGTH_LONG).show();
                }


                Intent startDashboardIntent = new Intent(LoginActivity.this, DashboardActivity.class);
                //startDashboardIntent.putExtra(database.EMAIL_KEY, etEmail.getText().toString());
                //startDashboardIntent.putExtra(database.PASSWORD_KEY, etpW.getText().toString());
                // EMAIL_KEY  and pass fields IN DATABASE CLASS with what was input
                startActivity(startDashboardIntent); //but we need to send to database
                // to do handling of press on that button and call for Dashboard
            }
            else {
                showMessage(R.string.login_input_error);
                }


    }};
    private View.OnClickListener mOnRegisterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            LoginActivity.this.startActivity(registerIntent);
        }
    };


        private boolean isEmailValid(){
            return !TextUtils.isEmpty(editEmail.getText()) &&Patterns.EMAIL_ADDRESS.matcher(editEmail.getText()).matches();
        }
        private boolean isPasswordValid(){return !TextUtils.isEmpty(editPw.getText());
        }
        private void showMessage (@StringRes int string){
            Toast.makeText(this, string, Toast.LENGTH_LONG).show();
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

          editEmail = findViewById(R.id.etEmail);// why final not private?
          editPw = findViewById(R.id.etPw);
         Button bLogin = findViewById(R.id.bLogin);
         Button bRegister = findViewById(R.id.bReg);

        bLogin.setOnClickListener(mOnLoginClickListener);
        bRegister.setOnClickListener(mOnRegisterClickListener);


               //
            }
        }



