package com.example.dan.studentapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {
private CardView timeCard, libCard, moodCard, floorCard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //defining Cards
        timeCard = (CardView) findViewById(R.id.time_card);
        libCard = (CardView) findViewById(R.id.lib_card);
        moodCard = (CardView) findViewById(R.id.mood_card);
        floorCard = (CardView) findViewById(R.id.floor_card);
        // Add Click Listener to the cards
        timeCard.setOnClickListener(this);
        libCard.setOnClickListener(this);
        moodCard.setOnClickListener(this);
        floorCard.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.mnuDashboard) {
            Toast.makeText(this, "Dashboard menu is Selected", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this,DashboardActivity.class));
        }
        else
        if(id==R.id.mnuTimetable) {
            Toast.makeText(this, "Timetable menu is Selected", Toast.LENGTH_SHORT).show();
        }
        else
        if(id==R.id.mnuLibrary) {
            Toast.makeText(this, "Library menu is Selected", Toast.LENGTH_SHORT).show();
        }
        else
        if(id==R.id.mnuMoodle) {
            Toast.makeText(this, "Moodle menu is Selected", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this,MoodleActivity.class));
        }
        else
        if(id==R.id.mnuFloorPlan) {
            Toast.makeText(this, "Floor Plan menu is Selected", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.commonmenus,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.lib_card : i = new Intent(this,LoginActivity.class);startActivity(i); break;
            case R.id.time_card : i = new Intent(this,LoginActivity.class);startActivity(i); break;
            case R.id.mood_card : i = new Intent(this,MoodleActivity.class);startActivity(i); break;
            case R.id.floor_card : i = new Intent(this,FloorPlanActivity.class);startActivity(i); break;

            default:break;
        }

    }

}
