package com.example.dan.studentapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLCon extends SQLiteOpenHelper // Class from android library to work with databases comes with
        //with two abstract methods onCreate and onUpgrade
        //also we need to create constructor for SQLco class matching superior class Helper
{
    public static final String DBNAME ="roedb.db"; //final because name of database is not changeble
    public static int VERSION =1 ; // later we may add changes into db and version will be changed by onUpgrade for example
    SQLiteDatabase db; //creation of database

    public SQLCon(Context context) // SQLCon class creating object which is to be instantiated by constructor method SQLCon
    //who calls method constructor super who in turn open parent class SQLiteOpenHelper and database created and tables are created in onCreate

    {
        super(context, DBNAME, null, VERSION);   // super  corresponds to parent class Helper constructor
        db =getWritableDatabase(); //repeated step to create data base where student can write data
    }                               // for insert queries get writable to be disabled as comment

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String CREATEGROUPSTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Groups.TABLE_NAME_6 + "( " +
                UniContractSQL.Groups.GROUP_ID + " varchar(45) PRIMARY KEY);";
        db.execSQL(CREATEGROUPSTABLE);

        final String CREATESTUDENTTABLE = "CREATE Table IF NOT EXISTS " +
                UniContractSQL.Student.TABLE_NAME + "( " +
                UniContractSQL.Student.ID + " INTEGER PRIMARY KEY autoincrement," +
                UniContractSQL.Student.NAME + " varchar(45) NOT NULL," +
                UniContractSQL.Student.SURNAME + " varchar(45) NOT NULL," +
                UniContractSQL.Groups.GROUP_ID + " varchar(45) NOT NULL," +// not from student class
                UniContractSQL.Student.PASS + " varchar(45) NOT NULL," +
                UniContractSQL.Student.EMAIL + " varchar(45) NOT NULL," +
                "FOREIGN KEY (_group_id_) REFERENCES __groups_ (_group_id_));";
        db.execSQL(CREATESTUDENTTABLE); //method to execute query (OBJECT) called CREATESTUDENTTABLE
        Log.d("CREATESTUDENTTABLE", CREATESTUDENTTABLE);//IS TO BE REPEATED FOR EVERY TABLE?

        final String CREATEAUTHORTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Author.TABLE_NAME_2 + "( " +
                UniContractSQL.Author.AUID + " INTEGER  PRIMARY KEY AUTOINCREMENT," +
                UniContractSQL.Author.AUNAME + " varchar(45) NOT NULL," +
                UniContractSQL.Author.AUSURNAME + " varchar(45) NOT NULL);";
        db.execSQL(CREATEAUTHORTABLE);

        final String CREATECATEGORYTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Category.TABLE_NAME_4 + "( " +
                UniContractSQL.Category.BOOK_CATEGORY_TYPE + " varchar(45) PRIMARY KEY);";
        db.execSQL(CREATECATEGORYTABLE);

        final String CREATEBOOKTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Books.TABLE_NAME_3 + "( " +
                UniContractSQL.Books.ISBN + " varchar(45) PRIMARY KEY," +
                UniContractSQL.Books.BOOK_TITLE + " varchar(45) NOT NULL," +
                UniContractSQL.Books.EDITION + " int(11) NOT NULL," +
                UniContractSQL.Category.BOOK_CATEGORY_TYPE + " varchar(45) NOT NULL," +
                "FOREIGN KEY (_category_type_) REFERENCES _category_ (_category_type_));";
        db.execSQL(CREATEBOOKTABLE);

        final String CREATEMODULETABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Module.Table_Name_5 + "( " +
                UniContractSQL.Module.MODULE_CODE + " INTEGER PRIMARY KEY," +
                UniContractSQL.Module.MODULE_NAME + " varchar(45) NOT NULL);";
        db.execSQL(CREATEMODULETABLE);

        final String CREATELESSONTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Lessons.TABLE_NAME_7 + "( " +
                UniContractSQL.Groups.GROUP_ID + " varchar(45) NOT NULL," +
                UniContractSQL.Lessons.DATE_ + " date NOT NULL," +
                UniContractSQL.Module.MODULE_CODE + " int(11) NOT NULL," +
                UniContractSQL.Lessons.TIME_ + " time NOT NULL," +
                "FOREIGN KEY (_group_id_) REFERENCES _groups_ (_group_id_)," +
                "FOREIGN KEY (_module_code_) REFERENCES _module_ (_module_code_));";
        db.execSQL(CREATELESSONTABLE);

        final String CREATEHASWRITTENTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.HasWritten.TABLE_NAME_8 + "( " +
                UniContractSQL.Books.ISBN + " varchar(45) PRIMARY KEY," +//PRIMARY KEY??????????????????????
                UniContractSQL.Author.AUID + " int(11) NOT NULL," +// composit primary key
                "FOREIGN KEY (_author_id_) REFERENCES _author_ (_author_id_)," +
                "FOREIGN KEY (_ISBN_) REFERENCES _book_ (_ISBN_));";
        db.execSQL(CREATEHASWRITTENTABLE);

        final String CREATEREVIEWTABLE = "CREATE TABLE IF NOT EXISTS " +
                UniContractSQL.Reviews.TABLE_NAME_9 + "( " +
                UniContractSQL.Reviews.REVIEW_ID + " INTEGER PRIMARY KEY," +
                UniContractSQL.Student.ID + " int(11) NOT NULL," +
                UniContractSQL.Reviews.REVIEW + " varchar(200) NOT NULL," +
                UniContractSQL.Books.ISBN + " varchar(45) NOT NULL," +
                "FOREIGN KEY (_ISBN_) REFERENCES _book_(_ISBN_)," +
                "FOREIGN KEY (_id_) REFERENCES _student_(_id_));";
        db.execSQL(CREATEREVIEWTABLE);
    }
    public void insertData(SQLiteDatabase db,String editFn , String editSn, String groupChoice, String editEmail, String editPw)
        {

            ContentValues contentValues = new ContentValues();
            contentValues.put(UniContractSQL.Student.NAME, editFn);// take sname from registration class
            contentValues.put(UniContractSQL.Student.SURNAME, editSn);
            contentValues.put(UniContractSQL.Groups.GROUP_ID, groupChoice);
            contentValues.put(UniContractSQL.Student.EMAIL, editEmail);
            contentValues.put(UniContractSQL.Student.PASS, editPw);

            db.insert(UniContractSQL.Student.TABLE_NAME, UniContractSQL.Groups.TABLE_NAME_6, contentValues);// click on insert to see if
            //inserted or not return -1 not inserted
        }
//Checking if email exists for login activity
    public Boolean checkData(SQLiteDatabase db, String email, String pass){
        Cursor cursor = db.rawQuery("Select * from _student_ where _email_=? and _pass_=?", new String[]{email, pass});
        if (cursor.getCount()>0) return false;
        else return true;
    }




    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

