package com.example.dan.studentapp;

import android.content.ClipData;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Spinner;

import java.util.ArrayList;

public class Library extends AppCompatActivity {
    ArrayList<BookSelect> items;
    Spinner sCategory;//spinner needed to sort books by category with new loop on array list and store in arraylistsortedbycategory
    //add array to populate spinner in values-strings.xml
    RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        sCategory = (Spinner) findViewById(R.id.bookcatspinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        public ArrayList<BookSelect> getitems(){
             BookSelect bookSelect = new BookSelect();
             ArrayList<BookSelect> items = new ArrayList<>();//items was data
            SQLCon con =new SQLCon(Library.this);
            SQLiteDatabase db = con.getWritableDatabase();
           Cursor cursor = db.rawQuery("select * from _book_ ;",null);//joint with _has_written_ on _isbn_ and _author_id_ from _author_

           StringBuffer stringBuffer = new StringBuffer();
            BookSelect bookSelect = null;
           cursor.moveToNext() {
               bookSelect= new BookSelect();
               String category = cursor.getString(cursor.getColumnIndexOrThrow("_category_type_"));
                String btitle = cursor.getString(cursor.getColumnIndexOrThrow("_title_"));
                String author = cursor.getString(cursor.getColumnIndexOrThrow("_sn_"));
                    String edition = cursor.getString(cursor.getColumnIndexOrThrow("_edition_"));
                    String isbn = cursor.getString(cursor.getColumnIndexOrThrow("_isbn_"));

                     bookSelect.setCategory(category);
                     bookSelect.setBtitle(btitle);
                     bookSelect.setAuthor(author);
                      bookSelect.setEdition(edition);
                     bookSelect.setIsbn(isbn);
               stringBuffer.append(bookSelect);
                 stringBuffer.append(bookSelect);
               items.add(bookSelect);
           }



            return items;
       }

        BookAdapter adapter = new BookAdapter(this, items);

        //setting adapter to recyclerview
        recyclerView.setAdapter(adapter);

    }
}
